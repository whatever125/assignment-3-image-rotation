#include "image.h"
#include "bmpio.h"
#include "rotate.h"
#include <stdio.h>
#include <string.h>

#define OK_CODE 0
#define ERROR_CODE (-1)

int main(int argc, char** argv) {
  if (argc != 4) {
    fprintf(stderr, "Incorrect number of arguments. Usage example: ./image-transformer <source-image> <transformed-image> <angle>\n");
    return ERROR_CODE;
  }
  if (strcmp(argv[3], "0") != 0 && strcmp(argv[3], "90") != 0 && strcmp(argv[3], "-90") != 0 && 
      strcmp(argv[3], "180") != 0 && strcmp(argv[3], "-180") != 0 && strcmp(argv[3], "270") != 0 && strcmp(argv[3], "-270") != 0) {
    fprintf(stderr, "Incorrect angle. The angle can take values strictly from the list: 0, 90, -90, 180, -180, 270, -270\n");
    return ERROR_CODE;
  }

  struct image image = {0};

  switch (read_bmp(argv[1], &image)) {
    case READ_OK:
      break;
    case READ_INVALID_SIGNATURE:
      fprintf(stderr, "Error reading BMP content");
      image_destroy(&image);
      return ERROR_CODE;
    case READ_INVALID_BITS:
      fprintf(stderr, "Error reading file");
      image_destroy(&image);
      return ERROR_CODE;
    case READ_INVALID_HEADER:
      fprintf(stderr, "Error reading BMP header");
      image_destroy(&image);
      return ERROR_CODE;
    default:
      fprintf(stderr, "Error reading");
      image_destroy(&image);
      return ERROR_CODE;
  }

  int64_t number_of_rotations = strtol(argv[3], NULL, 10) / 90;
  if (number_of_rotations < 0) {
    number_of_rotations = 4 + number_of_rotations;
  }
  for (size_t i = 0; i < number_of_rotations; i ++) {
    rotate(&image);
  }

  switch (write_bmp(argv[2], &image)) {
    case WRITE_OK:
      image_destroy(&image);
      break;
    case WRITE_ERROR:
      fprintf(stderr, "Error writing new file");
      image_destroy(&image);
      return ERROR_CODE;
    default:
      fprintf(stderr, "Error writing");
      image_destroy(&image);
      return ERROR_CODE;
  }

  return OK_CODE;
}
