#include <stdio.h>

#include <bmp.h>

enum read_status read_bmp(const char* filename, struct image* img) {
  FILE* file = fopen(filename, "rb");
  if (file == NULL) {
    return READ_INVALID_BITS;
  }
  enum read_status status = from_bmp(file, img);
  fclose(file);
  return status;
}

enum write_status write_bmp(const char* filename, struct image* img) {
  FILE* file = fopen(filename, "wb");
  if (file == NULL) {
    return WRITE_ERROR;
  }
  enum write_status status = to_bmp(file, img);
  fclose(file);
  return status;
}
