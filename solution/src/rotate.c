#include <image.h>

void rotate(struct image* source) {
  if (source->data == NULL) return;
  struct image result = image_create(source->height, source->width);
  if (!result.valid) return;
  for (size_t i = 0; i < source->height; i ++) {
    for (size_t j = 0; j < source->width; j ++) {
      size_t new_i = source->width - 1 - j;
      size_t new_j = i;
      result.data[new_i * result.width + new_j] = source->data[i * source->width + j];
    }
  }
  image_destroy(source);
  *source = result;
}
