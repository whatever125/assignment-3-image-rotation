#ifndef ROTATE_H
#define ROTATE_H

#include <image.h>

void rotate(const struct image* source);

#endif
