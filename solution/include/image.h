#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
  uint8_t b, g, r;
};

struct image {
  bool valid;
  uint64_t width, height;
  struct pixel* data;
};
static const struct image INVALID_IMAGE = { .valid = false };

struct image image_create(uint64_t width, uint64_t height);
void image_destroy(struct image* image);
struct pixel* image_pixel_at(const struct image image, uint64_t width, uint64_t height);

#endif
